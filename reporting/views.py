from django.shortcuts import render
from rest_framework import viewsets, filters, mixins
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from reporting.models import Error
from reporting.serializers import ErrorSerializer

# Create your views here.
class ErrorViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Error.objects.all()
    serializer_class = ErrorSerializer
