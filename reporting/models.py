import uuid
from django.db import models


# Create your models here.
class Application(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Error(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    state = models.TextField(blank=True,null=True)
    trace = models.TextField(blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    application = models.ForeignKey(Application, related_name="application")
