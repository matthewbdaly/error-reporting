from rest_framework import serializers
from reporting.models import Error

class ErrorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Error
        fields = '__all__'
