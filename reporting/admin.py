from django.contrib import admin
from reporting import models

# Register your models here.
admin.site.register(models.Application)
admin.site.register(models.Error)
